//paquete donde se encuentra esta clase
package com.ejemplo01;

import java.util.ArrayList;

/**
 * @author Jonathan Hernandez
 * Peque�a documentacion sobre la sintaxis basica de Java
 * Fecha: 08/01/2019
 */
//clase con modificador de acceso publico llamada Main
public class Main {
	
	/**
	 * Modificadores: son palabras reservadas que cambian el comportamiento de clases, metodos o variables
	 * 
	 * De acceso:
	 *   private    -> solo accesible dentro de la clase que se encuentre
	 *   public     -> accesible para cualquier clase
	 *   protected  -> accesible para las clases heredadas
	 *   sin asignar-> por default en java se establece privado
	 *   
	 * De no acceso:
	 *   static     -> establece que el miembro pertenece a la clase y no esta ligado a su instancia
	 *                 Por lo que no es necesario crear una instancia de la clase para poder usarse
	 *                 Se establece de manera fija en una posicion de la memoria por lo que cualquier 
	 *                 cambio se ve reflejada en todos los usos de este
	 *   final      -> indica que el elemento no se va a modificar (util para crear constantes)
	 *   abstract   -> cuando se usa en clases crea una clase abstracta la cual no puede ser instanciada
	 *   			   contiene metodos abstractos los cuales solo son definidos pero sobreescritos en las clases heredadas
	 *   
	 */
	
	/**
	 * Tipos primitivos: estan predefinidas por el lenguaje
	 * Se declaran mediante palabras reservadas
	 * No comparten estados entre con otras variables primitivas
	 */
	byte primitive_byte 	= 0b11;	 //utiliza 8  bits y tiene un rango de -127 a 128
	short primitive_short   = 1234;  //utiliza 16 bits y tiene un rango de -32,768 a 32,768
	int primitive_integer 	= 12345; //utiliza 32 bits y tiene un rango de -2^31 a 2^31
	long primitive_long 	= 123453242342l; //utiliza 64 bits y tiene un rango de -2^63 a 2^63
	float primitive_float	= 12.3f; //tiene una precision de 32 bits con decimales utiliza el literal f al final para asignar
	double primitive_double	= 12.34d;//tiene una precision de 64 bits con decimales utiliza el literal d al final para asignar
	char primitive_char 	= 'a';	 //utiliza 16 bits sin signo y permite almacenar un caracter de tipo unicode
	boolean primitive_bool  = false; //almacena unicamente estados false = 0 o true = 1
	
	/**
	 * Tipos compuestos: son objetos que contienen metodos, algunos de estos definidos en java son:
	 */
	
	//definidos en la biblioteca estandar de java
	String cadena="Hola mundo"; // almacena una cadena de texto de longitud variable
	ArrayList<String> lista_textos; //proporcionado por la libreria java.util define una lista del tipo asignado dentro del operador diamante
	//tipos definidos por el usuario son definiciones de clases 
	class Producto{ /**atributos y metodos**/}
	//arrays: serie de elementos tipo vector o matriz carece de metodos
	int numeros[] = {1,2,3,4};
	//tipo envoltorio Wrapper almacenan tipos primitivos pero provee funcionalidades
	Integer object_int = 123;
	
	//enumeracion contiene un conjunto de valores constantes
	public enum Day {
	    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	    THURSDAY, FRIDAY, SATURDAY 
	}
	/**
	 * Funcion de acceso publico que es estatica, es la primera funcion que se llama al ejecutar la app
	 * @param args arreglo de argumentos de entrada cuando se ejecuta
	 */
	public static void main(String[] args) {
		
		int arreglo_numerico[] = {1,2,3,4,5,6,7};
		
		//int mayor = (a,b)->{a>b?a:b};
		
		//recorriendo mediante el for estandar
		System.out.println("For estandar");
		for (int i = 0; i < arreglo_numerico.length; i++) {
			System.out.print(arreglo_numerico[i]+" ");
		}
		//recorriendo mediante el foreach
		System.out.println("\nForeach");
		for (int n:arreglo_numerico) {
			System.out.print(n+" ");
		}
		//recorriendo mediante el foreach
		System.out.println("\nutilizando continue");
		for (int n:arreglo_numerico) {
			if(n==2) continue;//vuelve al inicio del bucle
			System.out.print(n+" ");
		}
		
		//metodos numericos aplicados aun Wrapper tipo entero
		Integer myNumber = 5;
		//compare to - compara el valor contenido por ese objeto y retorna 1 de ser menor, 0 de ser igual y -1 de ser mayor
		System.out.println("\nutilizando compare to 5 - 1 "+myNumber.compareTo(1));
		System.out.println("utilizando compare to 5 - 5 "+myNumber.compareTo(5));
		System.out.println("utilizando compare to 5 - 5 "+myNumber.compareTo(10));
		
		//equals - compara el valor contenido y retorna false de ser distintos o true se der iguales
		System.out.println("utilizando equals to 5 - 1 "+myNumber.equals(1));
		System.out.println("utilizando equals to 5 - 5 "+myNumber.equals(5));
		System.out.println("utilizando equals to 5 - 5 "+myNumber.equals(10));
		
		//valueOf conversion de datos y retorna el tipo compuesto dada una entrada en otro formato
		//a la entrada tipo string se le puede anexar la base de ese numero a convertir
		myNumber = myNumber.valueOf("100", 16); //a base hexadecimal a decimal
		System.out.println("utilizando value of "+myNumber);
		myNumber = myNumber.valueOf("100", 10); //base decimal a base decimal
		System.out.println("utilizando value of "+myNumber);
		myNumber = myNumber.valueOf("100", 8); //base octal a decimal
		System.out.println("utilizando value of "+myNumber);
		myNumber = myNumber.valueOf("100", 2); //base binaria a decimal
		System.out.println("utilizando value of "+myNumber);
		
		//toString convierte el valor del objeto en una representacion de cadena de texto
		//en el caso numerico se puede establecer la base a la cual se va a representar
		String cad = myNumber.toString() + myNumber.toString();//concatena cada String
		System.out.println("utilizando el toString sobre un entero "+cad);
		
		//parseInt convierte una cadena de texto en un primitivo de tipo entero
		//se puede pasar como argumento la base en la cual esta esa cadena (retorna la representacion en decimal)
		
		int num = Integer.parseInt("100"); // a base decimal
		System.out.println("utilizando el parseInt sobre un entero "+num);

		num = Integer.parseInt("100",2); // de base binaria a decimal
		System.out.println("utilizando el parseInt sobre un entero "+num);
		
		
		//trabajando con cadenas
		//caracteres de escape: son precedidos por una pleca invertida y posee un significado cuando se ejecuta
		//en el compilador, se insertan en los string
		
		/*	\t	tabulacion
		 * 	\b	espacio en blanco
		 * 	\n	nueva linea
		 * 	\r 	retorno de carro
		 * 	\'	inserta comilla simple en la cadena de texto
		 * 	\"	inserta comilla doble en la cadena de texto
		 * 	\\	inserta pleca en la cadena de texto
		 */
		
		String c_escape = "Texto\tPrueba\tUtilizando\nCaracteres\tde\tescape\n\'comilla\"doble\\pleca";
		System.out.println(c_escape);
		
		//variable primitiva de tipo char
		char caracter = 'A';
		/*Character es un objeto que contiene funciones publicas estaticas que evaluan un caracter
		 * ya sea determinando si es un digito,letra, letra mayuscula, letra minuscula,etc
		 */
		System.out.println("funcion isDigit     sobre caracter "+caracter+" resultado "+Character.isDigit(caracter));
		System.out.println("funcion isLetter    sobre caracter "+caracter+" resultado "+Character.isLetter(caracter));
		System.out.println("funcion isLowerCase sobre caracter "+caracter+" resultado "+Character.isLowerCase(caracter));
		
		
		String texto ="Hola Mundo";
		//funcion toUpperCase retorna el texto en mayusculas
		System.out.println("Funcion toUpperCase sobre texto "+texto+" : "+texto.toUpperCase());
		System.out.println("Funcion toLowerCase sobre texto "+texto+" : "+texto.toLowerCase());
		
		//instancia de la clase Main
		Main main1 = new Main();
		Main main2 = new Main();
		
		//metodo to string retorna una representacion en texto del objeto main1
		System.out.println(main1.toString()); 
		//metodo to string retorna una representacion en texto del objeto main2 
		System.out.println(main2.toString());
		/*cada representacion de objetos es distinta y unica dentro de la aplicacion*/
		
		//evalua el contenido de la enumeracion y determina si es dia de trabajo o no
		if( itsAWorkDay(Day.SUNDAY) ) {
			System.out.println("Es dia de trabajo");
		}else {
			System.out.println("No es dia de trabajo");
		}
		
		//implementacion de singleton
		
		SingleUser.getInstance().setDescription("usuario administrativo");
		SingleUser.getInstance().setUsername("admin");
		
		System.out.println("User        : "+SingleUser.getInstance().getUsername());
		System.out.println("Description : "+SingleUser.getInstance().getDescription());
		
	}
	
	/**
	 * Determina si es dia de trabajo en base a una enumeracion
	 * @param d Dia de la semana
	 * @return falso si el dia es sabado o domingo
	 */
	public static boolean itsAWorkDay(Day d) {
		return !(d==Day.SATURDAY || d==Day.SUNDAY); 
	}

}
