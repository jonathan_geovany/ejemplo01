package com.ejemplo01;

//patron singleton
public class SingleUser {
	//variables privadas de tipo string
	private String username;
	private String description;
	//variable estatica de tipo SingleUser que contendra la instancia de la clase
	private static SingleUser instance=null;
	
	/**
	 * Constructor de tipo privado que limita su uso a unicamente el ambito de esta clase
	 */
	private SingleUser() {
		//inicializacion de los parametros
		username = description = "";
	}
	
	/**
	 * funcion estatica public que retorna la instancia de la clase
	 * en caso de que no exista una instancia se crea llamando al constructor
	 * @return instancia de la clase SingleUser
	 */
	public static SingleUser getInstance() {
		if(instance==null) instance = new SingleUser();
		return instance;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
